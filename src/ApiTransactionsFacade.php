<?php

namespace Itul\ApiTransactions;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\ApiTransactions\Skeleton\SkeletonClass
 */
class ApiTransactionsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'api-transactions';
    }
}
