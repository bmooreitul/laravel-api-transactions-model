<?php

namespace Itul\ApiTransactions;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Artisan;

class ApiTransactionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'laravel-api-transactions-model');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-api-transactions-model');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('api-transactions.php'),
            ], 'itul-api-transaction-assets');

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/laravel-api-transactions-model'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/laravel-api-transactions-model'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/laravel-api-transactions-model'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);

            //AUTO PUBLISH THE ASSETS
            Artisan::call('vendor:publish', ['--tag' => 'itul-api-transaction-assets']);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'api-transactions');

        // Register the main class to use with the facade
        $this->app->singleton('api-transactions', function () {
            return new ApiTransactionsFacade;
        });
    }
}
