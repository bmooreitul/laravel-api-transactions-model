<?php

namespace Itul\ApiTransactions;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Record extends Model {

    use HasFactory;

    protected $table = 'api_transactions';

    protected $fillable = [
        'api_name',
        'message',
        'context',
        'success',
        'type',
        'transaction',
        'parentable_id',
        'parentable_type',
        'connectionable_id',
        'connectionable_type',
        'trace',
        'direction',
    ];

    protected $casts = [
        'success'           => 'boolean',
        'context'           => 'json',
        'transaction'       => 'json',
        'trace'             => 'json',
    ];

    public static function boot(){
        parent::boot();

        static::creating(function($model){

        });
    }

    public static function create(array $attributes = []){

        $trace = debug_backtrace(0);
        array_shift($trace);
        $attributes['trace'] = array_values($trace);

        $attributes['context'] = [
            'url'       => @request()->url(),
            'auth'      => @auth()->check() ? auth()->user()->email : null,
            'cli'       => @php_sapi_name() === 'cli',
            'method'    => @request()->method(),
            'ajax'      => @request()->ajax(),
            'input'     => @request()->all(),
            'headers'   => @request()->header(),
            'session'   => @session()->all(),   
        ];

        //INSTANTIATE AND STORE THE NEW RECORD
        $res = (new \Itul\ApiTransactions\Record)->fill($attributes);
        $res->save();

        //RETURN THE RECORD
        return $res;
    }

    public static function make($apiName = 'none', array $clientParams = []){        

        //INSTANTIATE THE GUZZLE CLIENT
        $client = new \GuzzleHttp\Client($clientParams);

        //SEND BACK A CUSTOM CLASS
        return new class($client, $apiName) extends \GeneralObject{

            protected $_client;
            protected $_params;
            protected $_requestType = 'get';
            protected $_returnContent = true;

            //INITIALIZE AN API REQUET
            public function __construct(\GuzzleHttp\Client $client, string $apiName = 'none'){
                $this->_client = $client;
            }

            //RUN AN API REQUEST
            private function _run(string $requestType = 'get', string $uri, array $params = []){

                //DEFAULT RUN VARIABLES
                $http           = null;
                $parsedContent  = null;
                $message        = "An unknown error occurred";
                $success        = false;
                $requestTypes   = ['get' => 'read', 'post' => 'update', 'delete' => 'delete', 'rawPost' => 'update'];

                //TRY TO RUN THE REQUEST
                try{
                    $http           = $this->_client->{$requestType}($uri, $params);
                    $parsedContent  = $this->_parseContent($http);
                    $success        = true;
                    $message        = "Success";
                }

                //HANDLE REQUEST ERRORS
                catch(\Throwable $e){

                    //SET THE REQUEST REPONSE VARIABLES
                    $success        = false;
                    $message        = $e->getMessage();                    

                    //CHECK IF THE REQUEST RESPONSE CAN USE THE getResponse METHOD
                    if(method_exists($e, 'getResponse')){
                        $http           = $e->getResponse();
                        $tmpResponse    = json_decode($e->getResponse()->getBody()->getContents(), true);
                        $parsedContent  = $tmpResponse;
                    }
                }

                //CHECK IF THE RESULT CONTENT WA PARSED
                if(is_null($parsedContent)){
                    $message = $success ? "No records found" : $message;
                    $success = false;
                }

                //CREATE THE TRANSACTION RECORD
                $apiTransaction = \Itul\ApiTransactions\Record::create([
                    'api_name'          => $apiName,
                    'message'           => $message,
                    'success'           => $success,
                    'type'              => $requestTypes[$requestType],
                    'transaction'       => (object)[
                        'request'       => (object)[
                            'url'       => $uri,
                            'type'      => $requestType,
                            'params'    => $params,
                            'http'      => [
                                'headers'   => !is_null($http) ? @$http->getHeaders() : [],
                                'status'    => !is_null($http) ? @$http->getStatusCode() : 500,
                                'protocol'  => !is_null($http) ? @$http->getProtocolVersion() : null,
                                'body'      => $parsedContent,
                            ],
                        ],
                        'response'      => (object)[
                            'parsed'    => $parsedContent,
                        ],
                    ],
                    'direction'         => 'outbound',
                ]);

                return $this->_returnContent ? $apiTransaction->parsedResponse : $apiTransaction;                
            }

            //PARSE THE CONTENT
            private function _parseContent(\Psr\Http\Message\ResponseInterface $response){
                return json_decode($response->getBody()->getContents());
            }

            //GET/SET CONTENT RETURN
            public function returnContent($val = true){
                $this->_returnContent = $val;
                return $this;
            }

            //RUN A GET REQUEST
            public function get(string $uri, array $params = []){
                return $this->_run('get', $uri, $params);
            }

            //RUN A POST REQUEST
            public function post(string $uri, array $params = []){
                return $this->_run('post', $uri, $params);
            }

            //RUN A DELETE REQUEST
            public function delete(string $uri, array $params = []){
                return $this->_run('delete', $uri, $params);
            }

            //RUN A RAW POST REQUEST
            public function rawPost(string $uri, array $params = []){
                
                //CHECK IF THE BODY IS SET
                if(isset($params['body'])){

                    //FORCE THE BODY TO BE JSON ENCODED
                    if(!@json_decode($params['body'])) $params['body']  = json_encode($params['body']);

                    //FORCE HEADERS TO EXIST
                    if(!isset($params['headers'])) $params['headers']   = [];

                    //SET HEADERS FOR RAW POST
                    $params['headers']['Content-Type']                  = "application/json";
                    $params['headers']['Accept-Encoding']               = "gzip, deflate, br";
                    $params['headers']['Accept']                        = "application/json";
                }

                //RUN THE POST REQUEST
                return $this->_run('post', $uri, $params);
            }
        };
    }    

    //GET THE TRANSACTION OBJECT
    public function getTransactionObjAttribute(){
        return json_decode(json_encode($this->transaction));
    }

    //GET THE PARSED RESPONSE
    public function getParsedResponseAttribute(){
        return $this->transactionObj->response->parsed;
    }

    //PARENTABLE RELATIONSHIP
    public function parentable(){
        return $this->morphTo();
    }

    //CONNECTION RELATIONSHIP
    public function connectionable(){
        return $this->morphTo();
    }
}