<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAPITransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('api_name')->nullable();
            $table->boolean('success')->default(false);
            $table->string('type')->nullable();
            $table->longText('message')->nullable();
            $table->binary('context')->nullable();            
            $table->binary('transaction')->nullable();
            $table->nullableMorphs('parentable');
            $table->nullableMorphs('connectionable');
            $table->binary('trace')->nullable();
            $table->string('direction')->default('outbound');
            $table->timestamps();
        });

        \DB::statement("ALTER TABLE `api_transactions` CHANGE `context` `context` LONGBLOB NULL DEFAULT NULL");
        \DB::statement("ALTER TABLE `api_transactions` CHANGE `transaction` `transaction` LONGBLOB NULL DEFAULT NULL");
        \DB::statement("ALTER TABLE `api_transactions` CHANGE `trace` `trace` LONGBLOB NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_transactions');
    }
}
